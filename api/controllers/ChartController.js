/**
 * ChartController
 *
 * @description :: Server-side logic for managing charts
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {
	index : function(req,res){
		var dt_c = {
        name: 'Installation',
        data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
    };

		return res.view('Chart/index',{data_chart:dt_c});
	}
};
